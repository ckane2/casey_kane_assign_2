Name: Casey Kane
Email: ckane2@binghamton.edu

----------------------------------------------------------
## To clean:
ant -buildfile src/build.xml clean

----------------------------------------------------------
## To compile:
ant -buildfile src/build.xml all

Please compile before run. Needed to get BUILD and
BUILD/classes directories in directory structure
----------------------------------------------------------
## To run by specifying arguments from command line
ant -buildfile src/build.xml run -Darg0='path/to/input.txt' -Darg1='path/to/delete.txt' -Darg2='path/to/output1.txt' -Darg3='path/to/output2.txt' -Darg4='path/to/output3.txt'

----------------------------------------------------------
## To create tarball for submission
ant -buildfile src/build.xml tarzip

----------------------------------------------------------
## Academic Honesty Statement

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense."

[Date: ] -- 10/3/2017

----------------------------------------------------------
## Observer Pattern Explanation

The Observer pattern is implemented as follows:
1) Upon creation of backups, orig_node stores backups in ArrayList
2) Upon insertion or deletion, program checks if orig_node. If it is, 
notifyAll is called on all Observers of orig_node, if not skip 3.
3) notifyAll calls update on backup 1 and 2, which then call insert
or delete themselves (i.e- repeat step 2 for backups).

notifyAll is part of the SubjectI interface, and update
is part of ObserverI interface.

Node implements both SubjectI and ObserverI interfaces so
it provides implementations for both notifyAll and update.

----------------------------------------------------------
## Data Structure Justification

My data structure is a Red-Black Tree which balances the binary tree
on insertion but not on deletion. Therefore, unlike a BST which have
worst case O(n) for most operations, my data structure has a worst
case O(logn) insertion, deletion, and lookup time. For larger datasets
this will greatly improve the efficiency of my program.

----------------------------------------------------------
## Citations

Obtained and adapted algorithm for balanced insertion from:
www.geeksforgeeks.org/red-black-tree-set-2-insert/

Adapted rotation algorithm from:
www.geeksforgeeks.org/c-program-red-black-tree-insertion/
